import os
import shutil
import tarfile
import datetime

# Путь к директории с файлами
files_directory = '/tmp/test/files'

# Путь к директории с архивами
archives_directory = '/tmp/test/archives'

# Путь к лог-файлу
log_file = '/tmp/test/script-log/sync.log'

# Создание папки для архивов, если она не существует
os.makedirs(archives_directory, exist_ok=True)

# Функция для выбора последнего файла, не старше 7 дней
def get_last_modified_file(directory):
    files = [os.path.join(directory, file) for file in os.listdir(directory)]
    files = [file for file in files if os.path.isfile(file)]
    files = [file for file in files if os.path.getmtime(file) >= (datetime.datetime.now() - datetime.timedelta(days=7)).timestamp()]
    if files:
        return max(files, key=os.path.getmtime)
    return None

# Выбор последнего файла
last_modified_file = get_last_modified_file(files_directory)

if last_modified_file:
    # Получение информации о файле
    file_path = os.path.dirname(last_modified_file)
    file_name = os.path.basename(last_modified_file)
    file_size = os.path.getsize(last_modified_file)

    # Создание имени архива с использованием текущей даты и времени
    archive_name = f'{file_name}_{datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}.tar.gz'
    archive_path = os.path.join(archives_directory, archive_name)

    # Запись информации в лог-файл
    with open(log_file, 'a') as log:
        log.write(f'Date: {datetime.datetime.now()}\n')
        log.write(f'File Path: {file_path}\n')
        log.write(f'File Name: {file_name}\n')
        log.write(f'File Size: {file_size} bytes\n')
        log.write('\n')

    # Создание архива
    with tarfile.open(archive_path, 'w:gz') as tar:
        tar.add(last_modified_file, arcname=file_name)

    # Удаление исходного файла
    os.remove(last_modified_file)

    print(f'Файл успешно перемещен и удален.')

else:
    print('Нет файлов, удовлетворяющих условию.')
