#Описание
Скрипт по заданному расписанию выбирает последний измененный файл в директории /tmp/test/files но не старее 7 дней
Лог файла записывает в sync.log
Сжимает файл в tar.gz
Перемещает архив в archives
При успешном перемещении удаляет исходный файл
#
#ЗАПУСК
Положить скрипт в любое удобное место
Добавить в crontab 
*/15 * * * * python3 /path_to_file/changer.py
Либо используйте nohup
nohup bash -c "while true; do python3 /path_to_file/changer.py; sleep 900; done" &
#
